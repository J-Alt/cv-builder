import "./App.css";
import "./styles/btns.css";
import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Home from "./components/home/Home";
import Category from "./components/categories/Category";
import CvBuilder from "./components/cv-builder/CvBuilder";
import ErrorPage from "./components/reusable/ErrorPage";

const App = () => (
  <div className="App">
    <Router>
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route path="/category">
          <Category />
        </Route>
        <Route path="/web-development">
          <CvBuilder category="web-development" />
        </Route>
        <Route path="/data-science">
          <CvBuilder category="data-science" />
        </Route>
        <Route path="/digital-marketing">
          <CvBuilder category="digital-marketing" />
        </Route>
        <Route path="/design">
          <CvBuilder category="design" />
        </Route>
        <Route >
          <ErrorPage/>
        </Route>
      </Switch>
    </Router>
  </div>
);

export default App;
