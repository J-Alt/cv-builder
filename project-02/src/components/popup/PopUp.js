import React from "react";
import "../../styles/popup.css";

const PopUp = () => (
  <div className="pop-up">
    <div className="pop-up-wrapper">
      <div className="pop-up-title">
        <h1>Did you finnish all three? Way to go!</h1>
        <p className="pop-up-desc">
          Good luck on your next job! If you need help, counseling or just want
          to leave a suggestion, contact us at
        </p>
        <p className="pop-up-email">hello@wearelaika.com</p>
      </div>
    </div>
    <div className="pop-up-footer">
      <p>
        Created with &hearts; by the{" "}
        <a
          href="https://codepreneurs.brainster.co/"
          target="_blank"
          rel="noreferrer"
        >
          Brainster Coding Academy
        </a>{" "}
        students and{" "}
        <a href="https://www.wearelaika.com/" target="_blank" rel="noreferrer">
          wearelaika.com
        </a>
      </p>
    </div>
  </div>
);

export default PopUp;
