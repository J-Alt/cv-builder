import React from "react";

const Linkedin = (props) => (
  <div className="LinkedIn">
    <img
      src={require(`../../img/linkedin/bill1.png`).default}
      alt="img"
      useMap="#map1"
      width="716"
      height="536"
    />
    <map name="map1">
      <area
        shape="rect"
        coords="0,0,716,341"
        onMouseOver={(e) => props.tips(e, "Linkedin")}
        id="linkedin1"
        alt="linkedin tip"
      />
      <area
        shape="rect"
        coords="0,341,716,536"
        onMouseOver={(e) => props.tips(e, "Linkedin")}
        id="linkedin2"
        alt="linkedin tip"
      />
    </map>
    <img
      src={require(`../../img/linkedin/bill2.png`).default}
      alt="img"
      useMap="#map2"
      width="716"
      height="396"
    />
    <map name="map2">
      <area
        shape="rect"
        coords="0,0,716,396"
        onMouseOver={(e) => props.tips(e, "Linkedin")}
        id="linkedin3"
        alt="linkedin tip"
      />
    </map>
    <img
      src={require(`../../img/linkedin/bill3.png`).default}
      alt="img"
      useMap="#map3"
      width="716"
      height="488"
    />
    <map name="map3">
      <area
        shape="rect"
        coords="0,0,716,248"
        onMouseOver={(e) => props.tips(e, "Linkedin")}
        id="linkedin4"
        alt="linkedin tip"
      />
      <area
        shape="rect"
        coords="0,248,716,488"
        onMouseOver={(e) => props.tips(e, "Linkedin")}
        id="linkedin5"
        alt="linkedin tip"
      />
    </map>
    <img
      src={require(`../../img/linkedin/bill4.png`).default}
      alt="img"
      useMap="#map4"
      width="716"
      height="325"
    />
    <map name="map4">
      <area
        shape="rect"
        coords="0,0,716,248"
        onMouseOver={(e) => props.tips(e, "Linkedin")}
        id="linkedin6"
        alt="linkedin tip"
      />
    </map>
  </div>
);

export default Linkedin;
