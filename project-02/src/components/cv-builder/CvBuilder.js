import { useState } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import CvContextProvider from "../../context/cvContext";
import useTips from "../../hooks/useTips";
import Banner from "../reusable/Banner";
import CvNav from "../reusable/CvNav";
import Laika from "./Laika";
import Linkedin from "./Linkedin";
import ScrollBox from "./ScrollBox";
import Tips from "./Tips";
import Cv from "./Cv";
import PopUp from "../popup/PopUp";

const CvBuilder = (props) => {
  const tips = useTips(props.category);

  const [editMode, setEditMode] = useState(false);

  const handleEditMode = () => {
    setEditMode(true);
  };

  return (
    <Router>
      <CvContextProvider>
        <Switch>
          <Route path={`/${props.category}`}>
            <div className="CvBuilder">
              <div className="cv-builder-wrapper">
                <div className="cv-main-wrapper">
                  <CvNav category={props.category} />

                  <Route exact path={`/${props.category}/CV`}>
                    <Cv
                      category={props.category}
                      editMode={editMode}
                      handleEdit={handleEditMode}
                      tips={tips.getTips}
                    />
                  </Route>
                  <Route exact path={`/${props.category}/Linkedin`}>
                    <ScrollBox>
                      <Linkedin tips={tips.getTips} />
                    </ScrollBox>
                  </Route>
                  <Route exact path={`/${props.category}/Laika`}>
                    <ScrollBox>
                      <Laika tips={tips.getTips} />
                    </ScrollBox>
                  </Route>
                </div>
                <div className="cv-notes-wrapper">
                  <Tips tips={tips.displayTips.tip} />
                </div>
              </div>
              <Banner />
            </div>
          </Route>
          <Route exact path={`/pop-up`}>
            <PopUp />
          </Route>
        </Switch>
      </CvContextProvider>
    </Router>
  );
};

export default CvBuilder;
