import React from "react";

const Laika = (props) => (
  <div className="LinkedIn">
    <img
      src={require(`../../img/laika/Laika1.png`).default}
      alt="img"
      useMap="#map1"
      width="716"
      height="379"
    />
    <map name="map1">
      <area
        shape="rect"
        coords="0,198,468,259"
        onMouseOver={(e) => props.tips(e, "Laika")}
        id="laika1"
        alt="laika tip"
      />
    </map>
    <img
      src={require(`../../img/laika/Laika2.png`).default}
      alt="img"
      useMap="#map2"
      width="716"
      height="623"
    />
    <map name="map2">
      <area
        shape="rect"
        coords="0,0,716,432"
        onMouseOver={(e) => props.tips(e, "Laika")}
        id="laika2"
        alt="laika tip"
      />
      <area
        shape="rect"
        coords="0,425,716,623"
        onMouseOver={(e) => props.tips(e, "Laika")}
        id="laika3"
        alt="laika tip"
      />
    </map>
    <img
      src={require(`../../img/laika/laika3.png`).default}
      alt="img"
      useMap="#map3"
      width="716"
      height="612"
    />
    <map name="map3">
      <area
        shape="rect"
        coords="0,0,716,357"
        onMouseOver={(e) => props.tips(e, "Laika")}
        id="laika4"
        alt="laika tip"
      />
      <area
        shape="rect"
        coords="0,357,716,612"
        onMouseOver={(e) => props.tips(e, "Laika")}
        id="laika5"
        alt="laika tip"
      />
    </map>
    <img
      src={require(`../../img/laika/Laika4.png`).default}
      alt="img"
      useMap="#map4"
      width="716"
      height="540"
    />
    <map name="map4">
      <area
        shape="rect"
        coords="0,0,716,540"
        onMouseOver={(e) => props.tips(e, "Laika")}
        id="laika6"
        alt="laika tip"
      />
    </map>
    <img
      src={require(`../../img/laika/Laika5.png`).default}
      alt="img"
      useMap="#map5"
      width="716"
      height="601"
    />
    <map name="map5">
      <area
        shape="rect"
        coords="0,0,716,211"
        onMouseOver={(e) => props.tips(e, "Laika")}
        id="laika7"
        alt="laika tip"
      />
      <area
        shape="rect"
        coords="0,211,716,470"
        onMouseOver={(e) => props.tips(e, "Laika")}
        id="laika8"
        alt="laika tip"
      />
      <area
        shape="rect"
        coords="0,470,716,601"
        onMouseOver={(e) => props.tips(e, "Laika")}
        id="laika9"
        alt="laika tip"
      />
    </map>
  </div>
);

export default Laika;
