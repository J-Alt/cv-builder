import React from "react";
import "../../styles/scrollBox.css";

const ScrollBox = (props) => <div className="ScrollBox">{props.children}</div>;

export default ScrollBox;
