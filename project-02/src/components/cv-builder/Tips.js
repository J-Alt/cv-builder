import React from "react";

const Tips = (props) => (
  <div className="cv-notes-fixed">
    {props.tips ? (
      <p>{props.tips}</p>
    ) : (
      <div>
        <h4>CV:</h4>
        <p>Click on different titles to see tips for each category.</p>
        <h4>LinkedIn:</h4>
        <p>
          Scroll over different areas on the pictures to see tips for building
          your LinkedIn profile.
        </p>
        <h4>We Are LAIKA:</h4>
        <p>
          Scroll over different areas on the pictures to see tips for building
          your WEARELAIKA.COM profile
        </p>
      </div>
    )}
  </div>
);

export default Tips;
