import React, { useContext } from "react";
import { Link, withRouter } from "react-router-dom";
import { cvContext } from "../../../context/cvContext";
import "../../../styles/marketing.css";
import useCvToPdf from "../../../hooks/useCvToPdf";
import WorkExperience from "./small-cv-components/cv-section/WorkExperience";
import Education from "./small-cv-components/cv-section/Education";
import Personal from "./small-cv-components/cv-header/Personal";
import Photo from "./small-cv-components/cv-header/Photo";
import Contact from "./small-cv-components/cv-header/Contact";
import SkillsCompetencies from "./small-cv-components/cv-skills/SkillsCompetencies";
import Achievments from "./small-cv-components/cv-achievments/Achievments";
import Languages from "./small-cv-components/cv-language/Languages";
import InformalEd from "./small-cv-components/cv-informalEd/InformalEd";

const MarketingCv = React.forwardRef((props, ref) => {
  const toPDF = useCvToPdf();
  const myVal = useContext(cvContext);

  return (
    <div>
      {myVal.validate === true ? (
        <Link to="/pop-up">
          <button
            onClick={toPDF._exportPdf}
            className="btn btn-purple download-btn"
          >
            DOWNLOAD
          </button>
        </Link>
      ) : (
        <button
          onClick={toPDF._exportPdf}
          className="btn btn-purple download-btn tooltip disabled"
          disabled
        >
          <span className="tooltiptext">
            Please fill out at least 10 fields before download.
          </span>
          DOWNLOAD
        </button>
      )}
      <div className="marketing-cv" ref={toPDF.ref}>
        <header>
          <div className="flex-wrapper">
            <Photo tips={props.tips} />
            <Personal tips={props.tips} />
          </div>

          <Contact tips={props.tips} />
        </header>
        <section>
          <div className="left-section">
            <WorkExperience tips={props.tips} />
            <Education tips={props.tips} />
          </div>
          <div className="right-section">
            <SkillsCompetencies tips={props.tips} />
            <Achievments tips={props.tips} title="Achievments" />
            <Languages tips={props.tips} />
            <InformalEd tips={props.tips} />
          </div>
        </section>
      </div>
    </div>
  );
});

export default withRouter(MarketingCv);
