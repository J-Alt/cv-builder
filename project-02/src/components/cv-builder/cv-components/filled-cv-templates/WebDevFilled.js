import React from "react";
import "../../../../styles/webdev.css";

const WebDevFilled = (props) => {
  return (
    <div className="cv-contents web-development-cv">
      <header className="header">
        <div className="personal" id="cv1" onClick={(e) => props.tips(e, "CV")}>
          <div className="name-cont">
            <div className="name">Elon Musk</div>
            <div className="fixed-title">
              Entrepreneur, Engineer, Inventor and Investor
            </div>
          </div>
          <div className="about-fixed">
            Aiming to reduce global warming through sustainable energy
            production and consumption. Planning to reduce the risk of human
            extinction by making life multi-planetary and setting up a human
            colony on Mars.
          </div>
        </div>
        <div className="photo" id="cv3" onClick={(e) => props.tips(e, "CV")}>
          <img
            className="addImg"
            src={require(`../../../../img/cv/elon.jpg`).default}
            alt="Elon Musk"
          />
        </div>
        <div className="contact">
          <div id="cv4" onClick={(e) => props.tips(e, "CV")}>
            <div className={`contact-wrapper`}>
              <span>elon@teslamotors.com</span>
              <i className="fas fa-envelope"></i>
            </div>
            <div className={`contact-wrapper`}>
              <span>620-681-5000</span>
              <i className="fas fa-mobile-alt"></i>
            </div>
            <div className={`contact-wrapper`}>
              <span>Los Angeles, USA</span>
              <i className="fas fa-map-marker-alt"></i>
            </div>
            <div className={`contact-wrapper`}>
              <span>@elonmusk</span>
              <i className="fab fa-twitter"></i>
            </div>
          </div>
        </div>
      </header>
      <section className="body">
        <div className="left-section">
          <div
            className="work-section"
            id="cv5"
            onClick={(e) => props.tips(e, "CV")}
          >
            <h3>Work Experience </h3>
            <ul className="add-section">
              <li className="add-section-title">
                <div>Founder, CEO & Lead Designer</div>
              </li>
              <li className="company">
                <div>SpaceX-Space Exploration Technologies</div>
              </li>
              <li className="date">
                <span>06/2002</span> - <span>Present</span>
                <span>Howthrone, USA</span>
              </li>
              <li>
                <p className="fixed-acomplishments">Accomplishments</p>
              </li>
              <li>
                <ul className="tasks">
                  <li className="work-task">
                    <div>
                      Successfully lounched Falcon Heavy, the most powerful
                      operational rocket in the world by a factor of two, with
                      the ability to lift into orbit nearly 64 metric tons
                      (141.000 lb) -- a mass greater than a 737 jetliner loaded
                      with passengers, crew, luggage and fuel.
                    </div>
                  </li>
                  <li className="work-task">
                    <div>
                      Plans to reduce space transportation costs to enable
                      people to colonize Mars.
                    </div>
                  </li>
                  <li className="work-task">
                    <div>
                      Developed the Falcon 9 spacecraft which replaced the space
                      shuttle when it returned in 2011.
                    </div>
                  </li>
                </ul>
              </li>
            </ul>

            <ul className="add-section">
              <li className="add-section-title">
                <div>Founder</div>
              </li>
              <li className="company">
                <div>The Boring Company</div>
              </li>
              <li className="date">
                <span>12/2016</span> - <span>Present</span>
                <span>Howthrone, USA</span>
              </li>
              <li>
                <p className="fixed-acomplishments">Accomplishments</p>
              </li>
              <li>
                <ul className="tasks">
                  <li className="work-task">
                    <div>
                      Raised $10m by selling 20.000 flamethrowers in 4 days.
                    </div>
                  </li>
                  <li className="work-task">
                    <div>Raised $1m by selling 50.000 baseball caps.</div>
                  </li>
                  <li className="work-task">
                    <div>
                      Hyperloop - an ultra high-speed underground public
                      transportation system in which passangers are transported
                      on autonomous electric pods traveling at 600+ miles per
                      hour in a pressurized cabin.
                    </div>
                  </li>
                </ul>
              </li>
            </ul>

            <ul className="add-section">
              <li className="add-section-title">
                <div>CEO and Product Architect</div>
              </li>
              <li className="company">
                <div>Tesla Inc.</div>
              </li>
              <li className="date">
                <span>2004</span> - <span>Present</span>
                <span>San Mateo, USA</span>
              </li>
              <li>
                <p className="fixed-acomplishments">Accomplishments</p>
              </li>
              <li>
                <ul className="tasks">
                  <li className="work-task">
                    <div>
                      Global sales passed 250.000 units in Septemver 2017.
                    </div>
                  </li>
                  <li className="work-task">
                    <div>
                      Consumer Reports name Tesla as the top American car brand
                      and ranked it 8th among global carmakers in February 2017.
                    </div>
                  </li>
                  <li className="work-task">
                    <div>
                      Topped ConsumerReports Annual Owner Satisfaction Survey at
                      91% in 2016.
                    </div>
                  </li>
                </ul>
              </li>
            </ul>

            <ul className="add-section">
              <li className="add-section-title">
                <div>Co-founder and Former Chairman</div>
              </li>
              <li className="company">
                <div>SolarCity (subsidiary of Tesla Inc.)</div>
              </li>
              <li className="date">
                <span>2004</span> - <span>Present</span>
                <span>San Mateo, USA</span>
              </li>
              <li>
                <p className="fixed-acomplishments">Accomplishments</p>
              </li>
              <li>
                <ul className="tasks">
                  <li className="work-task">
                    <div>
                      Merged the company with Tesla Inc. and now offers energy
                      storage services through Tesla, including a turnkey
                      residential battery backup service that incorporates
                      Tesla's Powerwall.
                    </div>
                  </li>
                  <li className="work-task">
                    <div>
                      In 2015, installed 870MW of solar power, approximately 28%
                      of non-utility solar installation in US that year.
                    </div>
                  </li>
                </ul>
              </li>
            </ul>

            <ul className="add-section">
              <li className="add-section-title">
                <div>Founder & CEO</div>
              </li>
              <li className="company">
                <div>Neurolink</div>
              </li>
              <li className="date">
                <span>07/2016</span> - <span>Present</span>
                <span>San Francisco, USA</span>
              </li>
              <li>
                <p className="fixed-acomplishments">Vision</p>
              </li>
              <li>
                <ul className="tasks">
                  <li className="work-task">
                    <div>
                      A company aims to make devices to treat serious brain
                      diseases in the short-term, with the eventual goal of
                      human enhancement.
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
        <div className="right-section">
          <div
            className="skills-competencies"
            id="cv7"
            onClick={(e) => props.tips(e, "CV")}
          >
            <h3>Skills & Competencies</h3>
            <div>
              <ul className="Competencies">
                <li className="competence">
                  <div className="skill">Thinking through first principles</div>
                </li>
                <li className="competence">
                  <div className="skill">Marketing</div>
                </li>
                <li className="competence">
                  <div className="skill">Micromanagement</div>
                </li>
                <li className="competence">
                  <div className="skill">Goal oriented</div>
                </li>
                <li className="competence">
                  <div className="skill">Resiliency</div>
                </li>
                <li className="competence">
                  <div className="skill">Future focused</div>
                </li>
                <li className="competence">
                  <div className="skill">Leadership</div>
                </li>
                <li className="competence">
                  <div className="skill">Creativity</div>
                </li>
                <li className="competence">
                  <div className="skill">Time management</div>
                </li>
                <li className="competence">
                  <div className="skill">Persistence</div>
                </li>
                <li className="competence">
                  <div className="skill">Turning ideas into companiest</div>
                </li>
                <li className="competence">
                  <div className="skill">Long-term thinking</div>
                </li>
              </ul>
            </div>
          </div>
          <div
            className="achievments"
            id="cv8"
            onClick={(e) => props.tips(e, "CV")}
          >
            <h3>Achievments & Certificates </h3>

            <div className="achievment">
              <div className="achievment-title">
                <div>53rd Richest person in the world - Forbes (2018)</div>
              </div>
            </div>

            <div className="achievment">
              <div className="achievment-title">
                <div>
                  21st on the Forbes list of The World's Most Powerfull People
                  (2016)
                </div>
              </div>
            </div>

            <div className="achievment">
              <div className="achievment-title">
                <div>IEEE Honorary Membership (2015)</div>
              </div>

              <div className="achievment-desc">
                <div>
                  Given to people who have rendered meritorious service to
                  humanity in the IEEE's designated fields of interest.
                </div>
              </div>
            </div>
          </div>

          <div className="achievment">
            <div className="achievment-title">
              <div>Businessperson of The Year by Fortune Magazine (2013)</div>
            </div>
          </div>

          <div className="achievment">
            <div className="achievment-title">
              <div>FAI Gold Space Medal (2010)</div>
            </div>

            <div className="achievment-desc">
              <div>
                One of the highest honors in the aerospace industry shared with
                prominent personalities like Neil Armstrong and John Glenn
              </div>
            </div>
          </div>

          <div className="achievment">
            <div className="achievment-title">
              <div>
                Honorary Doctorate in Design from the Art Center College of
                Design
              </div>
            </div>
          </div>
          <div className="achievment">
            <div className="achievment-title">
              <div>
                Honorary Doctorate (DUniv) in Aerospace Engineering from the
                University of Surrey.
              </div>
            </div>
          </div>
          <div className="achievment">
            <div className="achievment-title">
              <div>
                Honorary Doctorate of Engineering and Technology from Yale
              </div>
            </div>
          </div>
          <div className="fixed-interests">
            <h3>Interests</h3>
            <div className="interests">
              <div className="interests-left">
                <div>
                  <i className="fas fa-atom"></i> <span>Physics</span>
                </div>
                <div>
                  <i className="fas fa-mountain"></i>{" "}
                  <span>Sustainability</span>
                </div>
                <div>
                  <i className="fas fa-globe-americas"></i>{" "}
                  <span>Philanthrophy</span>
                </div>
                <div>
                  <i className="fab fa-twitter"></i> <span>Twitter</span>
                </div>
                <div>
                  <i className="fas fa-user-astronaut"></i>{" "}
                  <span>Extraterrestrial Life</span>
                </div>
              </div>
              <div className="interests-right">
                <div>
                  <i className="fas fa-broadcast-tower"></i>{" "}
                  <span>Alternative energy</span>
                </div>
                <div>
                  <i className="fas fa-rocket"></i>{" "}
                  <span>Space Engineering</span>
                </div>
                <div>
                  <i className="fas fa-book"></i> <span>Reading</span>
                </div>
                <div>
                  <i className="fas fa-gamepad"></i> <span>Video Games</span>
                </div>
                <div>
                  <i className="fas fa-brain"></i> <span>AI</span>
                </div>
              </div>
            </div>
          </div>

          {/* _____ */}
        </div>
      </section>
    </div>
  );
};

export default WebDevFilled;
