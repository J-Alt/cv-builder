import React from "react";
import "../../../../styles/design.css";

const DesignFilled = (props) => {
  return (
    <div className="cv-contents design-cv">
      <header>
        <div className="header-wrapper">
          <div
            className="personal"
            id="cv1"
            onClick={(e) => props.tips(e, "CV")}
          >
            <div className="name-cont">
              <div className="name-wrapper">
                <div className="name">
                  <p>Kiril </p>
                  <p>Nikolovski</p>
                </div>

                <p className="title">Graphic designer </p>
              </div>
            </div>
            <div className="about">
              <p>
                Graphic Designer with expertise in branding and logo design,
                packaging design, typography, creative direction and
                illustration. Oriented into helping small businesses realize
                their unique vision.
              </p>
            </div>
          </div>
        </div>
      </header>
      <section>
        <div className="section-wrapper">
          <div className="design-left">
            <div className="left-wrapper">
              <div
                className="work-section"
                id="cv5"
                onClick={(e) => props.tips(e, "CV")}
              >
                <h3>Work Experience </h3>

                <ul className="add-section">
                  <li className="add-section-title">
                    <p>Junior Graphic Designer</p>
                  </li>
                  <li className="company">
                    <p>Brainster.co, Skopje.</p>
                  </li>
                  <li className="date">
                    <p>02.2019 -</p>
                  </li>
                  <li className="work-desc">
                    <p>
                      Creating visual concepts to communicate ideas that
                      inspire, inform and captivate consumers. Developing
                      overall layout and production design for ads, brochures,
                      magazines and corporate digital marketing content.
                    </p>
                  </li>
                </ul>

                <ul className="add-section">
                  <li className="add-section-title">
                    <p>Freelance Graphic Designer</p>
                  </li>
                  <li className="company">
                    <p>Upwork.</p>
                  </li>
                  <li className="date">
                    <p>2016 -2019</p>
                  </li>
                  <li className="work-desc">
                    <p>
                      Working with international clients and helping them get
                      their ideas out in a workable way. Conceptualizing visuals
                      based on their requirements, Shaping the visual aspects of
                      brand identities, product packaging, book covers,
                      magazines, brochures, posters and many more.
                    </p>
                  </li>
                </ul>
              </div>
              <h3>Contact</h3>
              <div className="contact-design">
                <p>xxxxxx</p>
                <p>kiril.nikolovskixx@gmail.com</p>
                <p>linkedin.com/in/kiril-nikolovski</p>
                <p>Skopje, Macedonia</p>
              </div>
            </div>
          </div>
          <div className="design-right">
            <div className="right-wrapper">
              <div
                className="education"
                id="cv6"
                onClick={(e) => props.tips(e, "CV")}
              >
                <h3>Education </h3>
                <ul className="add-section">
                  <li className="add-section-title">
                    <p>Brainster Academy of Design</p>
                  </li>
                  <li className="date">
                    <p>2018 -2019</p>
                  </li>
                </ul>
                <ul className="add-section">
                  <li className="add-section-title">
                    <p>
                      Ss. Cyril and Methodius University Faculty of Mechanical
                      Engineering
                    </p>
                    <p>Industrial Design</p>
                  </li>
                  <li className="date">
                    <p>2018 -2019</p>
                  </li>
                </ul>
              </div>
              <div
                className="skills-competencies"
                id="cv7"
                onClick={(e) => props.tips(e, "CV")}
              >
                <h3>Skills & Competencies</h3>
                <p>Adobe Photohop</p>
                <p>Adobe Illustrator</p>
                <p>Adobe InDesign</p>
                <p>Adobe Premiere Pro</p>
                <p>Adobe After Effects</p>
                <p>Adobe Xd</p>
                <p>MS Office</p>
                <p>SolidWorks</p>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default DesignFilled;
