import React from "react";
import "../../../../styles/dataScience.css";
import "../../../../styles/rating.css";

const DataScienceFilled = (props) => {
  return (
    <div className="data-science-cv">
      <header>
        <div className="flex-wrapper">
          <div className="photo" id="cv3" onClick={(e) => props.tips(e, "CV")}>
            <img
              src={require(`../../../../img/cv/aleksandra.jpg`).default}
              alt="Aleksandra"
              className="addImg"
            />
          </div>
          <div
            className="personal"
            id="cv1"
            onClick={(e) => props.tips(e, "CV")}
          >
            <div className="name-cont">
              <div className="name-wrapper">
                <div className="name">Aleksandra Janeva</div>
                <div className="title">Data Scientist</div>
              </div>
            </div>
            <div className="about">
              Highly accurate and experienced Data Scientist adept at collcting,
              analyzing, and interpreting large datasets, developing new
              forecasting models and performing data management tasks.
            </div>
          </div>
        </div>

        <div className="c-wrap" id="cv4" onClick={(e) => props.tips(e, "CV")}>
          <div className="contact-wrapper">
            <div className="wrap-cont-inp">
              <span>aleksandraxx@wearelaika.com</span>
              <i className="fas fa-envelope c"></i>
              <span>xxxxxx</span>
              <i className="fas fa-mobile-alt c"></i>
            </div>
          </div>
        </div>
      </header>
      <section>
        <div className="left-section">
          <div
            className="work-section"
            id="cv5"
            onClick={(e) => props.tips(e, "CV")}
          >
            <h3>Work Experience </h3>
            <ul className="add-section">
              <li className="add-section-title">
                <div>Data Scientist</div>
              </li>
              <li className="company">
                <div>Rocket Corp</div>
              </li>
              <li className="date">
                <span>11/2019</span> - <span>Present</span>
                <span>Skopje</span>
              </li>
              <li>
                <p className="fixed-acomplishments">Achievments/Tasks</p>
              </li>
              <li>
                <ul className="tasks">
                  <li className="work-task">
                    <div>
                      Collecting, analizing and interpreting raw data from
                      various websites
                    </div>
                  </li>
                  <li className="work-task">
                    <div>
                      Collaborating with the Operations and Technology
                      Department on the development of new automated data
                      management/analysis software which increases the overall
                      productivity and cut unnecessary costs.
                    </div>
                  </li>
                  <li className="work-task">
                    <div>Maintaining and managing company's MS SQL server</div>
                  </li>
                </ul>
              </li>

              <li className="add-section-title">
                <div>Data Scientist</div>
              </li>
              <li className="company">
                <div>Random Co.</div>
              </li>
              <li className="date">
                <span>07/2019</span> - <span>10/2019</span>
                <span>Skopje</span>
              </li>
              <li>
                <p className="fixed-acomplishments">Achievments/Tasks</p>
              </li>
              <li>
                <ul className="tasks">
                  <li className="work-task">
                    <div>
                      Reported actionable, statistical, and analytical insights
                      to executives for effective strategic positioning in the
                      market place
                    </div>
                  </li>
                  <li className="work-task">
                    <div>
                      Shadowed data scientists and assist in developing
                      algorithms for predictive modeling
                    </div>
                  </li>
                  <li className="work-task">
                    <div>
                      Analyzed and processed sophisticated data sets using SAS,
                      MySQL and Excel
                    </div>
                  </li>
                  <li className="work-task">
                    <div>Wrote python scripts to automate everyday tasks</div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
          <div
            className="education"
            id="cv6"
            onClick={(e) => props.tips(e, "CV")}
          >
            <h3>Education </h3>
            <ul className="add-section">
              <li className="add-section-title">
                <div>Data Science Academy</div>
              </li>
              <li className="company">
                <div>Brainster</div>
              </li>
              <li className="date">
                <span>01/2019</span> - <span>01/2020</span>
                <span>Skopje</span>
              </li>
            </ul>
            <ul className="add-section">
              <li className="add-section-title">
                <div>Financial management</div>
              </li>
              <li className="company">
                <div>
                  Faculty of Economy, University "St. Cyril and Methodius"
                </div>
              </li>
            </ul>
          </div>
        </div>
        <div className="right-section">
          <div
            className="skills-competencies"
            id="cv7"
            onClick={(e) => props.tips(e, "CV")}
          >
            <h3>Skills</h3>
            <div className="new-skill">
              <p>Python</p>
              <div className="rating">
                <input type="radio" name="rating" id={`circle1`} />
                <label htmlFor={`circle1`} id="1" className="colored"></label>
                <input type="radio" name="rating" id={`circle2`} />
                <label htmlFor={`circle2`} id="2" className="colored"></label>
                <input type="radio" name="rating" id={`circle3`} />
                <label htmlFor={`circle3`} id="3" className="colored"></label>
                <input type="radio" name="rating" id={`circle4`} />
                <label htmlFor={`circle4`} id="4" className="colored"></label>
                <input type="radio" name="rating" id={`circle5`} />
                <label htmlFor={`circle5`} id="5"></label>
              </div>
            </div>

            <div className="new-skill">
              <p>MySQL</p>
              <div className="rating">
                <input type="radio" name="rating" id={`circle1`} />
                <label htmlFor={`circle1`} id="1" className="colored"></label>
                <input type="radio" name="rating" id={`circle2`} />
                <label htmlFor={`circle2`} id="2" className="colored"></label>
                <input type="radio" name="rating" id={`circle3`} />
                <label htmlFor={`circle3`} id="3" className="colored"></label>
                <input type="radio" name="rating" id={`circle4`} />
                <label htmlFor={`circle4`} id="4" className="colored"></label>
                <input type="radio" name="rating" id={`circle5`} />
                <label htmlFor={`circle5`} id="5"></label>
              </div>
            </div>

            <div className="new-skill">
              <p>PHP</p>
              <div className="rating">
                <input type="radio" name="rating" id={`circle1`} />
                <label htmlFor={`circle1`} id="1" className="colored"></label>
                <input type="radio" name="rating" id={`circle2`} />
                <label htmlFor={`circle2`} id="2" className="colored"></label>
                <input type="radio" name="rating" id={`circle3`} />
                <label htmlFor={`circle3`} id="3" className="colored"></label>
                <input type="radio" name="rating" id={`circle4`} />
                <label htmlFor={`circle4`} id="4"></label>
                <input type="radio" name="rating" id={`circle5`} />
                <label htmlFor={`circle5`} id="5"></label>
              </div>
            </div>

            <div className="new-skill">
              <p>R</p>
              <div className="rating">
                <input type="radio" name="rating" id={`circle1`} />
                <label htmlFor={`circle1`} id="1" className="colored"></label>
                <input type="radio" name="rating" id={`circle2`} />
                <label htmlFor={`circle2`} id="2" className="colored"></label>
                <input type="radio" name="rating" id={`circle3`} />
                <label htmlFor={`circle3`} id="3" className="colored"></label>
                <input type="radio" name="rating" id={`circle4`} />
                <label htmlFor={`circle4`} id="4"></label>
                <input type="radio" name="rating" id={`circle5`} />
                <label htmlFor={`circle5`} id="5"></label>
              </div>
            </div>

            <div className="new-skill">
              <p>C</p>
              <div className="rating">
                <input type="radio" name="rating" id={`circle1`} />
                <label htmlFor={`circle1`} id="1" className="colored"></label>
                <input type="radio" name="rating" id={`circle2`} />
                <label htmlFor={`circle2`} id="2" className="colored"></label>
                <input type="radio" name="rating" id={`circle3`} />
                <label htmlFor={`circle3`} id="3" className="colored"></label>
                <input type="radio" name="rating" id={`circle4`} />
                <label htmlFor={`circle4`} id="4"></label>
                <input type="radio" name="rating" id={`circle5`} />
                <label htmlFor={`circle5`} id="5"></label>
              </div>
            </div>
          </div>

          <div
            className="achievments"
            id="cv8"
            onClick={(e) => props.tips(e, "CV")}
          >
            <h3>Projects and Publications</h3>
            <div className="achievment">
              <div className="achievment-title">
                <p>
                  The impact of Targeted Data Management Trainiing for Filed
                  Research Projects (06/2019
                </p>
              </div>
            </div>
          </div>

          <div
            className="languages"
            id="cv9"
            onClick={(e) => props.tips(e, "CV")}
          >
            <h3>Languages</h3>
            <p className="fixed-language">English</p>
            <p className="fixed-lvl">Full Proffesional Proficiency</p>
          </div>

          <div
            className="informal-ed"
            id="cv10"
            onClick={(e) => props.tips(e, "CV")}
          >
            <h3>Informal Education</h3>
            <ul className="informal-ed">
              <li className="informal">
                <div>Artificial Intelegence Webinar</div>
              </li>
              <li className="informal">
                <div>Business Analytics Course</div>
              </li>
            </ul>
          </div>
        </div>
      </section>
    </div>
  );
};

export default DataScienceFilled;
