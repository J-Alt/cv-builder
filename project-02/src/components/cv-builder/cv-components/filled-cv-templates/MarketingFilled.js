import React from "react";
import "../../../../styles/marketing.css";

const MarketingFilled = (props) => {
  return (
    <div className="marketing-cv">
      <header>
        <div className="flex-wrapper">
          <div className="photo" id="cv3" onClick={(e) => props.tips(e, "CV")}>
            <img
              src={require(`../../../../img/cv/stefanija.jpg`).default}
              alt="Stefanija"
              className="addImg"
            />
          </div>
          <div
            className="personal"
            id="cv1"
            onClick={(e) => props.tips(e, "CV")}
          >
            <div className="name-cont">
              <div className="name-wrapper">
                <div className="name">Stefanija Tenekedjieva</div>
                <div className="title">Digital Marketing</div>
              </div>
            </div>
            <div className="about">
              Fascinated by the power of communication and media. Quick learner.
              Endlessly curious and keen to know more.
            </div>
          </div>
        </div>

        <div className="c-wrap" id="cv4" onClick={(e) => props.tips(e, "CV")}>
          <div className="contact-wrapper">
            <div className="wrap-cont-inp">
              <span>stefanija.tenekedjieva@gmail.com</span>
              <i className="fas fa-envelope c"></i>
              <span>xxxxxx</span>
              <i className="fas fa-mobile-alt c"></i>
            </div>
          </div>
        </div>
      </header>
      <section>
        <div className="left-section">
          <div
            className="work-section"
            id="cv5"
            onClick={(e) => props.tips(e, "CV")}
          >
            <h3>Work Experience </h3>
            <ul className="add-section">
              <li className="add-section-title">
                <div>Digital Marketing</div>
              </li>
              <li className="company">
                <div>Rocket Corp</div>
              </li>
              <li className="date">
                <span>07/2019</span> - <span>Present</span>
                <span>Skopje, Macedonia</span>
              </li>
              <li>
                <p className="fixed-acomplishments">Achievments/Tasks</p>
              </li>
              <li>
                <ul className="tasks">
                  <li className="work-task">
                    <div>content writing</div>
                  </li>
                  <li className="work-task">
                    <div>social media advertising</div>
                  </li>
                  <li className="work-task">
                    <div>lead generation</div>
                  </li>
                  <li className="work-task">
                    <div>conversion rate optimisation</div>
                  </li>
                </ul>
              </li>

              <li className="add-section-title">
                <div>Project Coordinatopr</div>
              </li>
              <li className="company">
                <div>Radio MOF</div>
              </li>
              <li className="date">
                <span>12/2017</span> - <span>02/2019</span>
                <span>Skopje, Macedonia</span>
              </li>
              <li>
                <p className="fixed-acomplishments">Achievments/Tasks</p>
              </li>
              <li>
                <ul className="tasks">
                  <li className="work-task">
                    <div>
                      Editing, writing, planning and reporting for the projects
                      of radio mof
                    </div>
                  </li>
                  <li className="work-task">
                    <div>
                      Maintaining social media profiles, web content and
                      statistics of www.radiomof.mk through Google Analytics
                    </div>
                  </li>
                  <li className="work-task">
                    <div>
                      Editing and proofreading of content on www.radiomof.mk
                    </div>
                  </li>
                </ul>
              </li>
              <li className="add-section-title">
                <div>Journalist</div>
              </li>
              <li className="company">
                <div>Radio MOF</div>
              </li>
              <li className="date">
                <span>09/2015</span> - <span>12/2017</span>
                <span>Skopje, Macedonia</span>
              </li>
              <li>
                <p className="fixed-acomplishments">Achievments/Tasks</p>
              </li>
              <li>
                <ul className="tasks">
                  <li className="work-task">
                    <div>Content writing and reporting for Radio MOF</div>
                  </li>
                  <li className="work-task">
                    <div>Reports and articles for radio shows</div>
                  </li>
                  <li className="work-task">
                    <div>
                      Hosting trainings for Civil and Digital Journalism
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>

          <div
            className="education"
            id="cv6"
            onClick={(e) => props.tips(e, "CV")}
          >
            <h3>Education </h3>
            <ul className="add-section">
              <li className="add-section-title">
                <div>Digital Marketing Academy</div>
              </li>
              <li className="company">
                <div>Brainster</div>
              </li>
              <li className="date">
                <span>04/2019</span> - <span>09/2019</span>
                <span>Skopje, Mcedonia</span>
              </li>
              <li>
                <p className="fixed-acomplishments">Achievments/Tasks</p>
              </li>
              <li>
                <ul className="tasks fixed-flex">
                  <li className="work-task ed">
                    <div>Integrated Marketing Communication</div>
                  </li>
                  <li className="work-task ed ">
                    <div>Funnel Marketing</div>
                  </li>
                  <li className="work-task ed">
                    <div>Unpaid & Paid Social Media</div>
                  </li>
                  <li className="work-task ed">
                    <div>Sales</div>
                  </li>
                  <li className="work-task ed">
                    <div>Lead Generation</div>
                  </li>
                  <li className="work-task ed">
                    <div>Google Ads</div>
                  </li>
                  <li className="work-task ed">
                    <div>Google Analytics</div>
                  </li>
                  <li className="work-task ed">
                    <div>Growth Hacking</div>
                  </li>
                  <li className="work-task ed">
                    <div>Segmentation</div>
                  </li>
                  <li className="work-task ed">
                    <div>SEO</div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
        <div className="right-section">
          <div
            className="skills-competencies"
            id="cv7"
            onClick={(e) => props.tips(e, "CV")}
          >
            <h3>Skills</h3>
            <div>
              <ul className="Competencies">
                <li className="competence">
                  <div className="skill">Google Ads</div>
                </li>
                <li className="competence">
                  <div className="skill">Facebook Ads Manager</div>
                </li>
                <li className="competence">
                  <div className="skill">Google Analytics</div>
                </li>
                <li className="competence">
                  <div className="skill">Google Data Studio</div>
                </li>
                <li className="competence">
                  <div className="skill">Wordpress</div>
                </li>
                <li className="competence">
                  <div className="skill">Canva</div>
                </li>
                <li className="competence">
                  <div className="skill">Adobe Premiere</div>
                </li>
                <li className="competence">
                  <div className="skill">Content Writing</div>
                </li>
              </ul>
            </div>
          </div>

          <div
            className="achievments"
            id="cv8"
            onClick={(e) => props.tips(e, "CV")}
          >
            <h3>Achievments</h3>
            <div className="achievment">
              <div className="achievment-title">
                <p>
                  First Award for Professional reporting of the refugee crisis
                  in 2017 by UNHCR and MYLA
                </p>
              </div>
              <div className="achievment-title">
                <p>
                  Third award for Best journalistic story from the Macedonian
                  Council of Ethics in Media
                </p>
              </div>
            </div>
          </div>

          <div
            className="languages"
            id="cv9"
            onClick={(e) => props.tips(e, "CV")}
          >
            <h3>Languages</h3>
            <p className="fixed-language">English</p>
            <p className="fixed-lvl">Full Proffesional Proficiency</p>
          </div>
        </div>
      </section>
    </div>
  );
};

export default MarketingFilled;
