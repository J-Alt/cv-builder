import "../../../styles/design.css";
import React, { useContext } from "react";
import { Link, withRouter } from "react-router-dom";
import { cvContext } from "../../../context/cvContext";
import useCvToPdf from "../../../hooks/useCvToPdf";
import WorkExperience from "./small-cv-components/cv-section/WorkExperience";
import Education from "./small-cv-components/cv-section/Education";
import Personal from "./small-cv-components/cv-header/Personal";
import Contact from "./small-cv-components/cv-header/Contact";
import SkillsCompetencies from "./small-cv-components/cv-skills/SkillsCompetencies";
import Achievments from "./small-cv-components/cv-achievments/Achievments";
import Languages from "./small-cv-components/cv-language/Languages";

const DesignCv = React.forwardRef((props, ref) => {
  const toPDF = useCvToPdf();
  const myVal = useContext(cvContext);

  return (
    <div>
      {myVal.validate === true ? (
        <Link to="/pop-up">
          {" "}
          <button
            onClick={toPDF._exportPdf}
            className="btn btn-purple download-btn"
          >
            DOWNLOAD
          </button>
        </Link>
      ) : (
        <button
          onClick={toPDF._exportPdf}
          className="btn btn-purple download-btn tooltip disabled"
          disabled
        >
          <span className="tooltiptext">
            Please fill out at least 10 fields before download.
          </span>
          DOWNLOAD
        </button>
      )}
      <div className="cv-contents design-cv" ref={toPDF.ref}>
        <header>
          <div className="header-wrapper">
            <Personal tips={props.tips} />
          </div>
        </header>
        <section>
          <div className="section-wrapper">
            <div className="design-left">
              <div className="left-wrapper">
                <WorkExperience tips={props.tips} />
                <h3>Contact</h3>
                <Contact tips={props.tips} />
              </div>
            </div>
            <div className="design-right">
              <div className="right-wrapper">
                <Education tips={props.tips} />
                <Achievments
                  tips={props.tips}
                  title="Achievments & Certificates"
                />
                <SkillsCompetencies tips={props.tips} />
                <Languages tips={props.tips} />
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
  );
});

export default withRouter(DesignCv);
