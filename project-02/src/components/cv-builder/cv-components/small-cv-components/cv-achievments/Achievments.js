import React from "react";
import useFocus from "../../../../../hooks/useFocus";
import useDelete from "../../../../../hooks/useDelete";
import useAddNew from "../../../../../hooks/useAddNew";
import AddAchievment from "./AddAchievment";

const Achievments = (props) => {
  const focus = useFocus();
  const deleteEl = useDelete();
  const addNew = useAddNew();

  return (
    <div className="achievments" id="cv8" onClick={(e) => props.tips(e, "CV")}>
      <h3 onClick={focus.handleFocus}>{props.title}</h3>
      {focus.focused ? (
        <div className="tooltip inline">
          {" "}
          <i className="fas fa-plus-circle" onClick={addNew.addNew}></i>{" "}
          <i className="fas fa-times " onClick={deleteEl.handleDelete}></i>{" "}
          <span className="tooltiptext">
            Click on the "+" icon to add new or "X" icon to delete section.
            Click again to hide.
          </span>
        </div>
      ) : null}
      {addNew.items.map((el, i) => (
        <AddAchievment key={i} i={i} />
      ))}
    </div>
  );
};

export default Achievments;
