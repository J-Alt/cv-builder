import { useContext } from "react";
import useFocus from "../../../../../hooks/useFocus";
import useDelete from "../../../../../hooks/useDelete";
import { cvContext } from "../../../../../context/cvContext";
import Textarea from "react-expanding-textarea";
import AddAchievmentDesc from "./AddAchievmentDesc";

const AddAchievment = (props) => {
  const focus = useFocus();
  const deleteEl = useDelete();
  const myVal = useContext(cvContext);

  return (
    <div className="achievment">
      <div className="achievment-title" onClick={focus.handleFocus}>
        <Textarea
          placeholder="Achievment"
          name={`achievmentTitle${props.i}`}
          onChange={myVal.addNewVal}
          value={myVal.val[`achievmentTitle${props.i}`]}
        />
        {focus.focused ? (
          <div className="tooltip achievment-title-tooltip">
            {" "}
            <i
              className="fas fa-times "
              onClick={deleteEl.handleDelete}
            ></i>{" "}
            <span className="tooltiptext">Click to delete</span>
          </div>
        ) : null}
      </div>
      <AddAchievmentDesc i={props.i} />
    </div>
  );
};

export default AddAchievment;
