import { useContext } from "react";
import useFocus from "../../../../../hooks/useFocus";
import useDelete from "../../../../../hooks/useDelete";
import { cvContext } from "../../../../../context/cvContext";
import Textarea from "react-expanding-textarea";

const AddAchievmentDesc = (props) => {
  const focus = useFocus();
  const deleteEl = useDelete();
  const myVal = useContext(cvContext);

  return (
    <div className="achievment-desc" onClick={focus.handleFocus}>
      <Textarea
        placeholder="If needed write here description about achievment"
        name={`achievmentDesc${props.i}`}
        onChange={myVal.addNewVal}
        value={myVal.val[`achievmentDesc${props.i}`]}
      />
      {focus.focused ? (
        <div className="tooltip achievment-desc-tooltip">
          {" "}
          <i className="fas fa-times " onClick={deleteEl.handleDelete}></i>{" "}
          <span className="tooltiptext">Click to delete</span>
        </div>
      ) : null}
    </div>
  );
};

export default AddAchievmentDesc;
