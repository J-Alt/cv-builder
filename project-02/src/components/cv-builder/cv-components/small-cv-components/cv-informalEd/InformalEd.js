import React from "react";
import "../../../../../styles/tooltips.css";
import useFocus from "../../../../../hooks/useFocus";
import useDelete from "../../../../../hooks/useDelete";
import useAddNew from "../../../../../hooks/useAddNew";
import AddInformal from "./AddInformal";

const InformalEd = (props) => {
  const focus = useFocus();
  const deleteEl = useDelete();
  const addNew = useAddNew();

  return (
    <div className="informal-ed" id="cv10" onClick={(e) => props.tips(e, "CV")}>
      <h3 onClick={focus.handleFocus}>Informal Education</h3>
      {focus.focused ? (
        <div className="tooltip inline">
          {" "}
          <i className="fas fa-plus-circle" onClick={addNew.addNew}></i>{" "}
          <i className="fas fa-times " onClick={deleteEl.handleDelete}></i>{" "}
          <span className="tooltiptext">
            Click on the "+" icon to add new or "X" icon to delete section.
            Click again to hide.
          </span>
        </div>
      ) : null}

      {addNew.items.map((el, i) => (
        <AddInformal key={i} i={i} />
      ))}
    </div>
  );
};

export default InformalEd;
