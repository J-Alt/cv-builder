import { useContext } from "react";
import { cvContext } from "../../../../../context/cvContext";
import useFocus from "../../../../../hooks/useFocus";
import useDelete from "../../../../../hooks/useDelete";
import Textarea from "react-expanding-textarea";

const AddInformal = (props) => {
  const focus = useFocus();
  const deleteEl = useDelete();
  const myVal = useContext(cvContext);

  return (
    <div>
      <ul className="informal-ed">
        <li className="informal">
          <Textarea
            placeholder="Informal Education"
            onClick={focus.handleFocus}
            name={`informal${props.i}`}
            onChange={myVal.addNewVal}
            value={myVal.val[`informal${props.i}`]}
          />
          {focus.focused ? (
            <div className="tooltip informal-tooltip">
              {" "}
              <i
                className="fas fa-times "
                onClick={deleteEl.handleDelete}
              ></i>{" "}
              <span className="tooltiptext">Click to delete</span>
            </div>
          ) : null}
        </li>
      </ul>
    </div>
  );
};

export default AddInformal;
