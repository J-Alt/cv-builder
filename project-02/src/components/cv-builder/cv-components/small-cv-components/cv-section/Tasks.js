import { useContext } from "react";
import { cvContext } from "../../../../../context/cvContext";
import useFocus from "../../../../../hooks/useFocus";
import useAddNew from "../../../../../hooks/useAddNew";
import Textarea from "react-expanding-textarea";
import AddNewTask from "./AddNewTask";

const Tasks = (props) => {
  const focus = useFocus();
  const addNew = useAddNew();
  const myVal = useContext(cvContext);

  return (
    <div>
      <Textarea
        placeholder={props.plHolder}
        onClick={focus.handleFocus}
        className="tasks-title"
        name={`${props.item}`}
        onChange={myVal.addNewVal}
        value={myVal.val[`${props.item}`]}
      />
      {focus.focused ? (
        <div className="tooltip task-title-tooltip">
          {" "}
          <i className="fas fa-plus-circle" onClick={addNew.addNew}></i>{" "}
          <span className="tooltiptext">Click to add new {props.item}.</span>
        </div>
      ) : null}
      <ul className="tasks">
        {addNew.items.map((el, i) => (
          <AddNewTask key={i} i={el.id} item={props.item} />
        ))}
      </ul>
    </div>
  );
};

export default Tasks;
