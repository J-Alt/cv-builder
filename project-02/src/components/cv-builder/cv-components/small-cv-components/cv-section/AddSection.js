import { useContext } from "react";
import "../../../../../styles/tooltips.css";
import { cvContext } from "../../../../../context/cvContext";
import useFocus from "../../../../../hooks/useFocus";
import Tasks from "./Tasks";
import Textarea from "react-expanding-textarea";

const AddSection = (props) => {
  const focus = useFocus();
  const myVal = useContext(cvContext);

  return (
    <div className="add-section-wrapper" onClick={focus.handleFocus}>
      <ul className="add-section" onClick={focus.handleFocus}>
        <li className="add-section-title">
          <Textarea
            placeholder={props.title}
            name={`sectionTitle${props.i}${props.group}`}
            onChange={myVal.addNewVal}
            value={myVal.val[`sectionTitle${props.i}${props.group}`]}
          />
        </li>
        <li className="company">
          <Textarea
            placeholder={props.institution}
            name={`sectionInstitution${props.i}${props.group}`}
            onChange={myVal.addNewVal}
            value={myVal.val[`sectionInstitution${props.i}${props.group}`]}
          />
        </li>
        <li>
          <div className="date">
            <input
              type="text"
              placeholder="dd/mm/yy"
              name={`sectionDateFrom${props.i}${props.group}`}
              onChange={myVal.addNewVal}
              value={myVal.val[`sectionDateFrom${props.i}${props.group}`]}
            />{" "}
            <span>-</span>
            <input
              type="text"
              placeholder="dd/mm/yy"
              name={`sectionDateTo${props.i}${props.group}`}
              onChange={myVal.addNewVal}
              value={myVal.val[`sectionDateTo${props.i}${props.group}`]}
            />
            <Textarea
              type="text"
              placeholder="City, Country"
              className="city"
              name={`sectionCity${props.i}${props.group}`}
              onChange={myVal.addNewVal}
              value={myVal.val[`sectionCity${props.i}${props.group}`]}
            />
          </div>
        </li>
        <li>
          <Tasks plHolder={props.plHolder} item={props.item} />
        </li>
      </ul>
    </div>
  );
};

export default AddSection;
