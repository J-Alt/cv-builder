import React from "react";
import useFocus from "../../../../../hooks/useFocus";
import useDelete from "../../../../../hooks/useDelete";
import useAddNew from "../../../../../hooks/useAddNew";
import AddSection from "./AddSection";

const Education = (props) => {
  const focus = useFocus();
  const deleteEl = useDelete();
  const addNew = useAddNew();

  return (
    <div className="education" id="cv6" onClick={(e) => props.tips(e, "CV")}>
      <h3 onClick={focus.handleFocus}>Education </h3>
      {focus.focused ? (
        <div className="tooltip inline">
          {" "}
          <i className="fas fa-plus-circle" onClick={addNew.addNew}></i>{" "}
          <i className="fas fa-times " onClick={deleteEl.handleDelete}></i>{" "}
          <span className="tooltiptext">
            Click on the "+" icon to add new or "X" icon to delete section.
            Click again to hide.
          </span>
        </div>
      ) : null}
      {addNew.items.map((el, i) => (
        <AddSection
          key={i}
          plHolder="Studies/Courses"
          item="Thing learned"
          institution="School / Institution"
          title="Study program"
          i={i}
          group="education"
        />
      ))}
    </div>
  );
};

export default Education;
