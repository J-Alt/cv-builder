import React from "react";
import "../../../../../styles/tooltips.css";
import useFocus from "../../../../../hooks/useFocus";
import useDelete from "../../../../../hooks/useDelete";
import useAddNew from "../../../../../hooks/useAddNew";
import AddSection from "./AddSection";

const WorkExperience = (props) => {
  const focus = useFocus();
  const deleteEl = useDelete();
  const addNew = useAddNew();

  return (
    <div className="work-section" id="cv5" onClick={(e) => props.tips(e, "CV")}>
      <h3 onClick={focus.handleFocus}>Work Experience </h3>
      {focus.focused ? (
        <div className="tooltip">
          {" "}
          <i className="fas fa-plus-circle" onClick={addNew.addNew}></i>{" "}
          <i className="fas fa-times " onClick={deleteEl.handleDelete}></i>{" "}
          <span className="tooltiptext">
            Click on the "+" icon to add new or "X" icon to delete section.
            Click again to hide.
          </span>
        </div>
      ) : null}
      {addNew.items.map((el, i) => (
        <AddSection
          key={i}
          plHolder="Achievments/Tasks"
          item="Task"
          institution="Company/Workplace"
          title="Position/Title"
          i={i}
          group="work"
        />
      ))}
    </div>
  );
};

export default WorkExperience;
