import { useContext } from "react";
import "../../../../../styles/tooltips.css";
import { cvContext } from "../../../../../context/cvContext";
import useFocus from "../../../../../hooks/useFocus";
import useDelete from "../../../../../hooks/useDelete";
import Textarea from "react-expanding-textarea";

const AddNewTask = (props) => {
  const focus = useFocus();
  const deleteEl = useDelete();
  const myVal = useContext(cvContext);

  return (
    <li className="work-task">
      <Textarea
        placeholder={`${props.item} ${props.i}`}
        onClick={focus.handleFocus}
        name={`${props.item}No${props.i}`}
        onChange={myVal.addNewVal}
        value={myVal.val[`${props.item}No${props.i}`]}
      />
      {focus.focused ? (
        <div className="tooltip task-tooltip">
          {" "}
          <i className="fas fa-times " onClick={deleteEl.handleDelete}></i>{" "}
          <span className="tooltiptext">Click to delete.</span>
        </div>
      ) : null}
    </li>
  );
};

export default AddNewTask;
