import React from "react";
import AddCompetence from "./AddCompetence";

const SkillsCompetencies = (props) => {
  return (
    <div
      className="skills-competencies"
      id="cv7"
      onClick={(e) => props.tips(e, "CV")}
    >
      <h3>Skills & Competencies</h3>
      <AddCompetence />
    </div>
  );
};

export default SkillsCompetencies;
