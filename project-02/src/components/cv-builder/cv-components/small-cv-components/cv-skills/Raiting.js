import { useState } from "react";
import "../../../../../styles/rating.css";

const Raiting = (props) => {
  const [clicked, setClicked] = useState(0);

  const handleClicked = (e) => {
    let id = parseInt(e.target.id);
    setClicked(id);
  };

  return (
    <div className="rating">
      <input type="radio" name="rating" id={`circle1-${props.i}`} />
      <label
        htmlFor={`circle1-${props.i}`}
        id="1"
        onClick={handleClicked}
        className={clicked - 1 >= 0 ? "colored" : null}
      ></label>
      <input type="radio" name="rating" id={`circle2-${props.i}`} />
      <label
        htmlFor={`circle2-${props.i}`}
        id="2"
        onClick={handleClicked}
        className={clicked - 2 >= 0 ? "colored" : null}
      ></label>
      <input type="radio" name="rating" id={`circle3-${props.i}`} />
      <label
        htmlFor={`circle3-${props.i}`}
        id="3"
        onClick={handleClicked}
        className={clicked - 3 >= 0 ? "colored" : null}
      ></label>
      <input type="radio" name="rating" id={`circle4-${props.i}`} />
      <label
        htmlFor={`circle4-${props.i}`}
        id="4"
        onClick={handleClicked}
        className={clicked - 4 >= 0 ? "colored" : null}
      ></label>
      <input type="radio" name="rating" id={`circle5-${props.i}`} />
      <label
        htmlFor={`circle5-${props.i}`}
        id="5"
        onClick={handleClicked}
        className={clicked - 5 >= 0 ? "colored" : null}
      ></label>
    </div>
  );
};

export default Raiting;
