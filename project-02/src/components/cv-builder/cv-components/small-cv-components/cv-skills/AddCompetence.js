import React from "react";
import useFocus from "../../../../../hooks/useFocus";
import useDelete from "../../../../../hooks/useDelete";
import useAddNew from "../../../../../hooks/useAddNew";

const AddCompetence = () => {
  const focus = useFocus();
  const deleteEl = useDelete();
  const addNew = useAddNew();

  return (
    <div>
      <ul className="Competencies">
        {addNew.items.map((el, i) => (
          <li className="competence" key={i}>
            <div
              className="skill"
              onClick={focus.handleFocus}
              contentEditable="true"
              suppressContentEditableWarning={true}
            >
              Skill
            </div>
            {focus.focused ? (
              <div className="tooltip">
                {" "}
                <i className="fas fa-plus-circle" onClick={addNew.addNew}></i>
                <i
                  className="fas fa-times "
                  onClick={deleteEl.handleDelete}
                ></i>{" "}
                <span className="tooltiptext">Add or delete skill.</span>
              </div>
            ) : null}
          </li>
        ))}
      </ul>
    </div>
  );
};

export default AddCompetence;
