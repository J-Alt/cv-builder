import React from "react";
import AddSkill from "./AddSkill";

const Skills = (props) => {
  return (
    <div
      className="skills-competencies"
      id="cv7"
      onClick={(e) => props.tips(e, "CV")}
    >
      <h3>Skills</h3>
      <AddSkill />
    </div>
  );
};

export default Skills;
