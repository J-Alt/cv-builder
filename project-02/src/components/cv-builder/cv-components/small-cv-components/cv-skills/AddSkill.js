import { useContext } from "react";
import { cvContext } from "../../../../../context/cvContext";
import Textarea from "react-expanding-textarea";
import useFocus from "../../../../../hooks/useFocus";
import useDelete from "../../../../../hooks/useDelete";
import useAddNew from "../../../../../hooks/useAddNew";
import Raiting from "./Raiting";

const AddSkill = () => {
  const focus = useFocus();
  const deleteEl = useDelete();
  const addNew = useAddNew();
  const myVal = useContext(cvContext);

  return (
    <div>
      {addNew.items.map((el, i) => (
        <div className="new-skill" key={i} onClick={focus.handleFocus}>
          <Textarea
            type="text"
            placeholder="Skill Name"
            name={`DSkill${i}`}
            value={myVal.val[`DSkill${i}`]}
            onChange={myVal.addNewVal}
          />
          <Raiting i={i} />

          {focus.focused ? (
            <div>
              {" "}
              <i className="fas fa-plus-circle" onClick={addNew.addNew}></i>
              <i
                className="fas fa-times "
                onClick={deleteEl.handleDelete}
              ></i>{" "}
            </div>
          ) : null}
        </div>
      ))}
    </div>
  );
};

export default AddSkill;
