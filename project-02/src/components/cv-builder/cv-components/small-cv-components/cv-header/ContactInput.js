import { useContext } from "react";
import { cvContext } from "../../../../../context/cvContext";
import useDelete from "../../../../../hooks/useDelete";
import useFocus from "../../../../../hooks/useFocus";
import Textarea from "react-expanding-textarea";

const ContactInput = (props) => {
  const focus = useFocus();
  const deleteEl = useDelete();
  const myVal = useContext(cvContext);

  return (
    <div className="contact-wrapper" onClick={focus.handleFocus}>
      <div className="wrap-cont-inp">
        <Textarea
          type="text"
          placeholder={props.plHolder}
          name={`contact${props.i}`}
          value={myVal.val[`contact${props.i}`]}
          onChange={myVal.addNewVal}
        />
        {props.icon}
        {focus.focused ? (
          <div className="tooltip delete-icon" onClick={deleteEl.handleDelete}>
            <i className="fas fa-times "></i>
            <span className="tooltiptext contact-tooltip">
              Remove this field
            </span>
          </div>
        ) : null}
      </div>
    </div>
  );
};

export default ContactInput;
