import { useContext } from "react";
import { cvContext } from "../../../../../context/cvContext";
import Textarea from "react-expanding-textarea";

const About = () => {
  const myVal = useContext(cvContext);

  return (
    <Textarea
      placeholder="About"
      className="about"
      name="about"
      value={myVal.val.about}
      onChange={myVal.addNewVal}
    />
  );
};

export default About;
