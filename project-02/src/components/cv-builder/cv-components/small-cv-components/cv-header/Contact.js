import { useState } from "react";
import ContactInput from "./ContactInput";

const Contact = (props) => {
  const [contactItems] = useState([
    {
      id: 1,
      plHolder: "E-mail",
      icon: <i className="fas fa-envelope c"></i>,
    },
    {
      id: 2,
      plHolder: "Phone number",
      icon: <i className="fas fa-mobile-alt c"></i>,
    },
    {
      id: 3,
      plHolder: "Location",
      icon: <i className="fas fa-map-marker-alt c"></i>,
    },
    {
      id: 4,
      plHolder: "Twitter",
      icon: <i className="fab fa-twitter c"></i>,
    },
    {
      id: 5,
      plHolder: "Linkedin",
      icon: <i className="fab fa-linkedin c"></i>,
    },
  ]);

  return (
    <div className="c-wrap" id="cv4" onClick={(e) => props.tips(e, "CV")}>
      {contactItems.map((el, i) => (
        <ContactInput plHolder={el.plHolder} icon={el.icon} key={i} i={el.id} />
      ))}
    </div>
  );
};

export default Contact;
