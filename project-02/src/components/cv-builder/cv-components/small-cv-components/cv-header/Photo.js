import { useState } from "react";

const Photo = (props) => {
  const [uploadedImg, setUploadedImg] = useState("");

  const handleUpload = (e) => {
    let myImg = e.target.files[0];
    setUploadedImg(URL.createObjectURL(myImg));
  };

  return (
    <div
      className="photo tooltip"
      id="cv3"
      onClick={(e) => props.tips(e, "CV")}
    >
      <label className="img-upload">
        <input
          type="file"
          accept="image/*"
          file={uploadedImg}
          multiple={false}
          onChange={handleUpload}
        />
        <img
          src={
            uploadedImg !== ""
              ? uploadedImg
              : require(`../../../../../img/cv/addimg.png`).default
          }
          alt="attach"
          className="addImg"
        />
        <span className="tooltiptext">
          For best results, add image in 1:1 ratio
        </span>
      </label>
    </div>
  );
};

export default Photo;
