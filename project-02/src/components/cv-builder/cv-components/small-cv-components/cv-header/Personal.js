import { useContext } from "react";
import About from "./About";
import { cvContext } from "../../../../../context/cvContext";
import Textarea from "react-expanding-textarea";

const Personal = (props) => {
  const myVal = useContext(cvContext);

  return (
    <div className="personal" id="cv1" onClick={(e) => props.tips(e, "CV")}>
      <div className="name-cont">
        <div className="name-wrapper">
          <Textarea
            placeholder="Full name"
            className="name"
            name="name"
            value={myVal.val.name}
            onChange={myVal.addNewVal}
          />
          <Textarea
            placeholder="Proffesional title"
            className="title"
            name="title"
            value={myVal.val.title}
            onChange={myVal.addNewVal}
          />
        </div>
      </div>
      <About />
    </div>
  );
};

export default Personal;
