import React from "react";
import useFocus from "../../../../../hooks/useFocus";
import useAddNew from "../../../../../hooks/useAddNew";
import AddLanguage from "./AddLanguage";

const Languages = (props) => {
  const focus = useFocus();
  const addNew = useAddNew();

  return (
    <div className="languages" id="cv9" onClick={(e) => props.tips(e, "CV")}>
      <h3 onClick={focus.handleFocus}>Languages</h3>
      {focus.focused ? (
        <div className="tooltip">
          {" "}
          <i className="fas fa-plus-circle" onClick={addNew.addNew}></i>{" "}
          <span className="tooltiptext">Add new language.</span>
        </div>
      ) : null}
      {addNew.items.map((el, i) => (
        <AddLanguage key={i} i={i} />
      ))}
    </div>
  );
};

export default Languages;
