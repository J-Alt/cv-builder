import { useState, useContext } from "react";
import { cvContext } from "../../../../../context/cvContext";
import useFocus from "../../../../../hooks/useFocus";
import useDelete from "../../../../../hooks/useDelete";
import Textarea from "react-expanding-textarea";

const AddLanguage = (props) => {
  const [lvls, setLvls] = useState({
    clicked: { id: 4, lvl: "Full Proffesional Proficiency" },
    level: [
      { id: 1, lvl: "Elementary Proficiency" },
      { id: 2, lvl: "Limited Working Proficiency" },
      { id: 3, lvl: "Professional Working Proficiency" },
      { id: 4, lvl: "Full Proffesional Proficiency" },
      { id: 5, lvl: "Native or Bilingual Proficiency" },
    ],
  });
  const focus = useFocus();
  const deleteEl = useDelete();
  const myVal = useContext(cvContext);

  const languageLvl = (e) => {
    let current = lvls.level.find((el) => el.id == e.target.id);
    setLvls({ ...lvls, clicked: current });
  };

  return (
    <div className="language">
      <Textarea
        placeholder="English"
        onClick={focus.handleFocus}
        onChange={myVal.addNewVal}
        name={`language${props.i}`}
        value={myVal.val[`language${props.i}`]}
      />
      {focus.focused ? (
        <div>
          {" "}
          <div className="tooltip language-tooltip">
            <i className="fas fa-times " onClick={deleteEl.handleDelete}></i>{" "}
            <span className="tooltiptext">Click to delete</span>{" "}
          </div>{" "}
          {lvls.level.map((el, i) => (
            <span className="lvl" onClick={languageLvl} id={el.id} key={i}>
              {el.id}/5
            </span>
          ))}
        </div>
      ) : null}
      <p>{lvls.clicked.lvl}</p>
    </div>
  );
};

export default AddLanguage;
