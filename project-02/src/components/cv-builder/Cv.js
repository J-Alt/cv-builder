import React from "react";
import DataScienceCv from "./cv-components/DataScienceCv";
import DataScienceFilled from "./cv-components/filled-cv-templates/DataScienceFilled";
import DesignCv from "./cv-components/DesignCv";
import DesignFilled from "./cv-components/filled-cv-templates/DesignFilled";
import MarketingCv from "./cv-components/MarketingCv";
import MarketingFilled from "./cv-components/filled-cv-templates/MarketingFilled";
import WebDevCv from "./cv-components/WebDevCv";
import WebDevFilled from "./cv-components/filled-cv-templates/WebDevFilled";

const Cv = (props) => {
  return (
    <div className={`cv cv-${props.category}`}>
      {" "}
      {props.editMode ? (
        props.category === "web-development" ? (
          <WebDevCv tips={props.tips} />
        ) : props.category === "design" ? (
          <DesignCv tips={props.tips} />
        ) : props.category === "digital-marketing" ? (
          <MarketingCv tips={props.tips} />
        ) : props.category === "data-science" ? (
          <DataScienceCv tips={props.tips} />
        ) : null
      ) : (
        <>
          <button
            className="btn btn-purple edit-btn"
            onClick={props.handleEdit}
          >
            EDIT
          </button>
          {props.category === "web-development" ? (
            <WebDevFilled tips={props.tips} />
          ) : props.category === "design" ? (
            <DesignFilled tips={props.tips} />
          ) : props.category === "digital-marketing" ? (
            <MarketingFilled tips={props.tips} />
          ) : props.category === "data-science" ? (
            <DataScienceFilled tips={props.tips} />
          ) : null}
        </>
      )}
    </div>
  );
};

export default Cv;
