import React from "react";
import "../../styles/home.css";
import { Link } from "react-router-dom";
import Btn from "../reusable/Btn";

const Home = () => (
  <div className="Home">
    <div className="home-wrapper">
      <div className="home-title">
        <h1>
          The Ultimate <br /> CV & Portfolio Check - List
        </h1>
        <p>
          Are you a Web Developer, Data Scientist, Digital Marketer or a
          Designer?
        </p>
        <p>
          Have your CV and protfolio in check and create a 5-star representation
          of your skills whith this guide.{" "}
        </p>
        <Link to="/category">
          {" "}
          <Btn title="STEP INSIDE" color="btn btn-purple" />
        </Link>
      </div>
      <div className="home-img">
        <img
          src={require(`../../img/bg/BackgroundFirstPage.png`).default}
          alt="home-page background"
        ></img>
      </div>
    </div>
    <div className="home-footer">
      <p>
        Created with &hearts; by the{" "}
        <a
          href="https://codepreneurs.brainster.co/"
          target="_blank"
          rel="noreferrer"
        >
          Brainster Coding Academy
        </a>{" "}
        students and{" "}
        <a href="https://www.wearelaika.com/" target="_blank" rel="noreferrer">
          wearelaika.com
        </a>
      </p>
    </div>
  </div>
);

export default Home;
