import React from "react";
import { Link } from "react-router-dom";
import Btn from "../reusable/Btn";

const CategoryCard = (props) => (
  <div className="CategoryCard">
    <h3>{props.title}</h3>
    <Link to={props.path}>
      <Btn title="CHOOSE" color="btn btn-purple" />
    </Link>
  </div>
);

export default CategoryCard;
