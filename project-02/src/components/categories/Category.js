import React from "react";
import "../../styles/categories.css";
import Banner from "../reusable/Banner";
import CategoryCard from "./CategoryCard";

const Category = () => (
  <div className="Category">
    <div className="category-wrapper">
      <h2>Choose your category</h2>
      <div className="cards-wrapper">
        <CategoryCard title="Web Development" path="/web-development/CV" />
        <CategoryCard title="Data Science" path="/data-science/CV" />
        <CategoryCard title="Digital Marketing" path="/digital-marketing/CV" />
        <CategoryCard title="Design" path="/design/CV" />
      </div>
    </div>
    <Banner />
  </div>
);

export default Category;
