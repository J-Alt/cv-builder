import React from "react";

const Btn = (props) => (
  <button className={`${props.color}`}>{props.title}</button>
);

export default Btn;
