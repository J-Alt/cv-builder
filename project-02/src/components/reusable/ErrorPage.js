import React from "react";
import { Link } from "react-router-dom";
import "../../styles/errorpage.css";

const ErrorPage = () => {
  return (
    <div className="ErrorPage">
      <div className="error-cont">
        <h1>:( ERROR 404...</h1>
        <h2 className="error-desc"> This page does not exist.</h2>
        <Link to="/">
          <h2 className="error-link">Go to Homepage</h2>
        </Link>
      </div>
    </div>
  );
};

export default ErrorPage;
