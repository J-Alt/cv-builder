import { useState } from "react";
import "../../styles/nav.css";
import { Link } from "react-router-dom";
import Btn from "./Btn";

const CvNav = (props) => {
  const [myBtns, setMyBtns] = useState({
    active: { path: `/${props.category}/CV`, title: "CV", id: 1 },
    btns: [
      { path: `/${props.category}/CV`, title: "CV", id: 1 },
      { path: `/${props.category}/Linkedin`, title: "LINKEDIN", id: 2 },
      { path: `/${props.category}/Laika`, title: "WEARELAIKA.COM", id: 3 },
    ],
  });

  function handleActive(i) {
    setMyBtns({ ...myBtns, active: myBtns.btns[i] });
  }

  function toggleActive(i) {
    if (myBtns.btns[i].id === myBtns.active.id) {
      return "btn btn-light active";
    } else {
      return "btn btn-light";
    }
  }

  return (
    <div className="CvNav">
      {myBtns.btns.map((el, i) => (
        <Link
          to={el.path}
          key={i}
          onClick={() => {
            handleActive(i);
          }}
        >
          <Btn title={el.title} color={toggleActive(i)} />
        </Link>
      ))}
    </div>
  );
};

export default CvNav;
