import React from "react";
import "../../styles/banner.css";

const Banner = () => (
  <div className="Banner">
    <div className="banner-yellow">
      <p>Do you want to learn hands-on digital skills?</p>
      <a
        className="btn btn-black"
        href="https://brainster.co/"
        target="_blank"
        rel="noreferrer"
      >
        VISIT BRAINSTER
      </a>
    </div>
    <div className="banner-blue">
      <p>Do you want to recieve job offers by IT companies?</p>
      <a
        className="btn btn-black"
        href="https://www.wearelaika.com/"
        target="_blank"
        rel="noreferrer"
      >
        VISIT LAIKA
      </a>
    </div>
  </div>
);

export default Banner;
