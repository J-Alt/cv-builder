import { createContext, useState, useEffect } from "react";

export const Context = createContext({});

const CvContextProvider = (props) => {
  const cvContent = {};

  const [val, setVal] = useState(cvContent);
  const [validate, setValidate] = useState(false);

  const addNewVal = (e) => {
    setVal({ ...val, [e.target.name]: e.target.value });
  };

  useEffect(() => {
    if (Object.keys(val).length >= 10) {
      setValidate(true);
    } else {
      setValidate(false);
    }
  }, [val]);

  const cvContext = {
    val,
    validate,
    addNewVal,
  };

  return (
    <Context.Provider value={cvContext}>{props.children}</Context.Provider>
  );
};

export default CvContextProvider;
export const cvContext = Context;
