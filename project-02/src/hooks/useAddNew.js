import { useState } from "react";

function useAddNew() {
  const [items, setItems] = useState([{ id: 1 }]);

  const addNew = () => {
    setItems([...items, { id: items.length + 1 }]);
  };

  return {
    items,
    addNew,
  };
}

export default useAddNew;
