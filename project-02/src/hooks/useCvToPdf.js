import React from "react";
import jsPDF from "jspdf";
import html2canvas from "html2canvas";

const useCvToPdf = () => {
  const ref = React.createRef();

  const _exportPdf = () => {
    html2canvas(ref.current, {
      scale: 4,
      logging: true,
      allowTaint: true,
    }).then(function (canvas) {
      let base64image = canvas.toDataURL("image/jpeg");
      console.log("Report Image URL: " + base64image);

      let imgWidth = 210;
      let pageHeight = 295;
      let imgHeight = (canvas.height * imgWidth) / canvas.width;
      let heightLeft = imgHeight;
      let doc = new jsPDF("p", "mm", "a4");
      let position = 10;

      let width = doc.internal.pageSize.getWidth();
      let height = doc.internal.pageSize.getHeight();
      doc.addImage(base64image, "JPEG", 10, 0, imgWidth - 20, imgHeight);
      heightLeft -= pageHeight;

      while (heightLeft >= 0) {
        position = heightLeft - imgHeight;
        doc.addPage();
        doc.addImage(
          base64image,
          "JPEG",
          10,
          position,
          imgWidth - 20,
          imgHeight
        );
        heightLeft -= pageHeight;
      }

      doc.save("myCV.pdf");
    });
  };

  return {
    _exportPdf,
    ref,
  };
};

export default useCvToPdf;
