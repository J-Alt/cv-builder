import { useState } from "react";

function useFocus() {
  const [focused, setFocused] = useState(false);

  const handleFocus = (e) => {
    e.stopPropagation();
    setFocused(!focused);
  };

  return {
    handleFocus,
    focused,
  };
}

export default useFocus;
