function useDelete() {
  const handleDelete = (e) => {
    e.target.parentElement.parentElement.remove();
  };

  return {
    handleDelete,
  };
}

export default useDelete;
