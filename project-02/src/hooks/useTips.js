import { useState } from "react";
import Notes from "../db/Notes";

function useTips(category) {
  const [allTips] = useState(Notes[category]);
  const [displayTips, setDisplayTips] = useState("");

  function getTips(e, pg) {
    let display = allTips[pg].find((el) => el.id === e.currentTarget.id);
    setDisplayTips(display);
  }

  return {
    allTips,
    displayTips,
    getTips,
  };
}

export default useTips;
